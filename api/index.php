<?php
// aceitando requisicoes externas
header('Access-Control-Allow-Origin: *');

// requisitando e instanciando as classes
require './library/Autoload.php';
autoload('Controller');
autoload('Slim_Slim');
\Slim\Slim::registerAutoloader();

$ctrl = new Controller();
$app = new Slim\Slim();

//setando o cabecalho da resposta para json
$app->response()->header('Content-Type', 'application/json;charset=utf-8');

$app->get('/', function(){
    echo '{"status":"401"}';
});

/*
 * SMARTWALLET
 * Metodo que altera o registro de algum laboratorio na plataforma SmartWallet
 */
$app->post('/attLaboratorio', function() use($app, $ctrl){
    $body = json_decode($app->request->getBody());
    
    $retorno = $ctrl->walletAtualizarConta($body);
    
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

/*
 * SMARTWALLET
 * Metodo que cadastra um novo laboratorio na plataforma
 */
$app->post('/addLaboratorio', function() use($app,$ctrl){
    $body = json_decode($app->request->getBody());
    
    $retorno = $ctrl->walletAddLaboratorio($body);
    
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

/*
 * NOTIFICACAO
 * Recebe todas as notificacoes vindas da mundipagg
 */
$app->get('/recebeNotificacao/mdPagg/1558', function() use($app, $ctrl){echo '404';});
$app->post('/recebeNotificacao/mdPagg/1558', function() use($app, $ctrl){
    $app->response()->header('Content-Type', 'text/html;charset=utf-8');
    // le a requisicao, decodifica para string, e por ultimo, transforma a string xml em objeto do php
    $xml = simplexml_load_string(html_entity_decode($_POST["xmlStatusNotification"]));
    
    // chama o metodo que trata as notificacoes
    $retorno = $ctrl->trataNotificacao($xml);
    
    // da o retorno ao servico da Mundipagg
    if($retorno){
        echo 'OK';
    }else{
        
    }
});

/*
 * FORMULARIO DE CADASTRO PRINCIPAL
 * Recebendo os dados e inserindo no banco
 * Retorna com o link para a pagina de pagamento
 */
$app->post('/enviaFormPrincipal/', function () use($app, $ctrl) {
    $body = json_decode($app->request->getBody());
    
    $retorno = $ctrl->salvaDadosFormulario($body);
    
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

/*
 * FORMULARIO DE CADASTRO PRINCIPAL 
 * Busca por CEP
 */
$app->get('/buscaCep/:cep/', function ($cep) use ($ctrl){
    $retorno = $ctrl->buscaCep($cep);
    
    //imprimi a resposta
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});

/*
 * FORMULARIO DE CADASTRO PRINCIPAL 
 * Busca por Laboratorios
 */
$app->get('/buscaLaboratorios/:id', function($id) use ($ctrl){
    $retorno = $ctrl->buscaLaboratorios($id);
    
    //imprimi a resposta
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});


/*
 * FORMULARIO DE CADASTRO PRINCIPAL 
 * Busca por Laboratorios
 */
$app->get('/calcFrete/:cep/:tipoEnvio', function($cep, $tipoEnvio) use ($ctrl){
    $retorno = $ctrl->calculaFrete($cep, $tipoEnvio);
    
    //imprimi a resposta
    echo json_encode($retorno, JSON_UNESCAPED_UNICODE);
});


$app->run();