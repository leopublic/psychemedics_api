<?php
autoload('Bd_Conexao');
class Models_Declaracao {

    public function buscaTransactionKey($key) {
        $con = getInstance();

        $sql = 'SELECT * FROM azoprfrm WHERE VdaPedTransSplitKey = :key AND OprFrmAuto = :id';
    }

    public function salvaChaveSplit($respSplit) {
        $con = getInstance();

        $sql = 'UPDATE azoprfrm SET VdaPedTransSplitKey = :splitKey WHERE OprFrmAuto = :id';

        $stmt = $con->prepare($sql);
        $stmt->bindParam(':splitKey', $respSplit->CreditFinancialMovementCollection[0]->FinancialMovementKey);
        $stmt->bindParam(':id', $respSplit->CreditFinancialMovementCollection[0]->ItemReference);

        $stmt->execute();

        unset($con);

        if($stmt->rowCount() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function enviaDeclaracao($dados, $response) {
        // instancia, prepara e monta o SQL
        $con = getInstance();
        $sql = $this->montaInsertDeclaracao($dados, $response);
        $stmt = $con->prepare($sql);

        // executa o SQL
        $stmt->execute();

        // recupera o ultimo indice inserido
        $id = $con->lastInsertId();

        // termina a conexao
        unset($con);

        return $id;
    }

    private function montaInsertDeclaracao($dados, $response) {
        if($dados->pgMetodo == 'Boleto'){
            $urlBoleto = $response['metodo']['url'];
        }else{
            $urlBoleto = '';
        }

        return 'INSERT INTO azoprfrm
		(OprFrmAuto,
		LocUndSigla,
		PesPesLabAuto,
		OprFrmNum,
		OprFrmStatus,
		OprFrmAutent,
		OprFrmDstInd,
		OprFrmDstEnvTp,
		OprFrmDstNome,
		OprFrmDstLogr,
		OprFrmDstBairro,
		OprFrmDstNum,
		OprFrmDstCompl,
		OprFrmDstCep,
		OprFrmDstUndAuto,
		OprFrmDstCidade,
		OprFrmDstFone,
		OprFrmDstEMail,
		OprFrmFluxoInd,
		OprTDrogaAuto,
		OprFrmDtHrIncl,
		OprFrmDoadorNome,
		OprFrmDoadorCPF,
		OprFrmDoadorObs,
		OprFrmColetaData,
		OprFrmColetaObs,
		OprColetaOrgAuto,
		OprColetaUsoAuto,
		PesContatoLabAuto,
		OprFrmTest1Nome,
		OprFrmTest1CPF,
		OprFrmAmstrDtRec,
		OprFrmAmstrDtRecIncl,
		OprFrmAmstrPesContatoAuto,
		OprFrmAmstrCCFNum,
		OprFrmAmstrCCFLote,
		OprFrmAmstrCCFPesContatoAuto,
		OprFrmAmstrDtEnvUsa,
		OprFrmAmstrFedex,
		OprFrmAmstrDtRecUsa,
		OprFrmRsltInd,
		OprFrmRsltDtRec,
		OprFrmRsltDtDig,
		OprFrmRsltDtLibUsa,
		OprFrmRsltDias,
		OprFrmRsltLabId,
		OprFrmRsltCompr,
		OprFrmRsltDtHrIncl,
		OprFrmRsltPesContatoAuto,
		OprFrmSdxCxaAuto,
		OprFrmLaudoMedVis,
		OprFrmLaudoMedVisOk,
		OprFrmLaudoMedVisDtHr,
		OprFrmLaudoLibEnv,
		OprFrmLaudoLibDt,
		OprFrmLaudoLibPesContatoAuto,
		OprFrmLaudoLibObs,
		OprFrmLaudoAssinContatoAuto,
		OprFrmLaudoDtImpr,
		OprFrmLaudoDtEnv,
		OprFrmMsg,
		OprFrmObsLaudo,
		OprFrmObsGerais,
		OprFrmTestInd,
		OprFrmRcltInd,
		OprFrmRcltFrmNum,
		OprFrmCobrInd,
		VdaPedIndPed,
		VdaPedFormPgto,
                VdaPedFormPgtoStatus,
		OprFrmPgtoInd,
		VdaPedNome,
		VdaPedDF,
		VdaPedEMail,
		VdaPedLogr,
		VdaPedBairro,
		VdaPedNum,
		VdaPedCompl,
		LocUndAuto,
		LocCidade,
		VdaPedCep,
		VdaPedFone,
		VdaPedCelular,
		VdaPedFluxoUsrTipo,
		VdaPedIndVdaCalc,
		VdaPedValVda,
		VdaPedValLaudo,
		VdaPedQdeVias,
		VdaPedValVias,
		VdaPedValTot,
		VdaPedDtVenc,
                VdaPedTransKey,
		VdaPedTransSplitKey,
                VdaPedTransSplitStatus,
                VdaPedTransBolUrl,
		VdaPedPrioEmis,
		VdaPedPriorCod,
		FlagExecCmd
		)
	VALUES
		(0,
		"'.$dados->lab->uf.'",
		'.$dados->lab->id.',
		"",
		"A",
		"",
		"'.$dados->laudo->envioLaudo.'",
		"'.  substr($dados->cobranca->tipoFrete, 0, 1).'",
		"'.$dados->notaFiscal->nome.'",
		"'.$dados->endInformado->logradouro.'",
		"'.$dados->endInformado->bairro.'",
		"'.$dados->endInformado->numero.'",
		"'.$dados->endInformado->complemento.'",
		"'.str_replace('-','', $dados->endInformado->cep).'",
		'.$this->buscaIdUf($dados->endInformado->uf).',
		"'.$dados->endInformado->cidade.'",
		"",
		"",
		0,
		0,
		now(),
		"'.$dados->doador->nome.'",
		"'.str_replace(array('.','-', ' '),array('','',''), $dados->doador->cpf).'",
		"",
		"0000-00-00",
		"",
		0,
		0,
		0,
		"",
		"",
		"0000-00-00",
		"0000-00-00 00:00:00",
		0,
		"",
		0,
		0,
		"0000-00-00",
		"",
		"0000-00-00",
		"A",
		"0000-00-00",
		"0000-00-00",
		"0000-00-00",
		0,
		"",
		"",
		"0000-00-00 00:00:00",
		0,
		0,
		"'.$dados->laudo->medicoVisualiza.'",
		"N",
		"0000-00-00 00:00:00",
		"N",
		"0000-00-00 00:00:00",
		0,
		"",
		0,
		"0000-00-00",
		"0000-00-00",
		"",
		"",
		"",
		"N",
		"N",
		"",
		"S",
		"'.$response['indPedido'].'",
		"'.substr($dados->pgMetodo, 0,1).'",
                "'.$response['metodo']['status'].'",
		"N",
		"'.$dados->notaFiscal->nome.'",
		"'.str_replace(array('.','-','/', ' '),array('','','',''), $dados->notaFiscal->doc).'",
		"'.$dados->notaFiscal->email.'",
		"'.$dados->endCobranca->logradouro.'",
		"'.$dados->endCobranca->bairro.'",
		"'.$dados->endCobranca->numero.'",
		"'.$dados->endCobranca->complemento.'",
		'.$this->buscaIdUf($dados->endCobranca->uf).',
		"'.$dados->endCobranca->cidade.'",
		"'.str_replace('-','', $dados->endCobranca->cep).'",
		"'.str_replace(array('(',')','-',' '), array('','','',''), $dados->notaFiscal->fone).'",
		"'.str_replace(array('(',')','-',' '), array('','','',''), $dados->doador->celular).'",
		"'.$dados->notaFiscal->envioFluxo.'",
		"S",
		'.str_replace(',','.', $dados->cobranca->exame).',
		'.str_replace(',','.', $dados->cobranca->frete).',
		'.$dados->laudo->quantidadeVias.',
		'.str_replace(',','.', $dados->cobranca->totalVias).',
		'.str_replace(',','.', $dados->cobranca->total).',
		"0000-00-00",
                "'.$response['metodo']['transactionKey'].'",
		"",
                "",
                "'.$urlBoleto.'",
		"A",
		"",
		"P"
		)';
    }

    private function buscaIdUf($uf) {
        if($uf == ''){
            return $uf;
        }

        $con = getInstance();
        $sql = 'SELECT LocUndAuto FROM locund WHERE LocUndSigla = "'.$uf.'"';
        $stmt = $con->prepare($sql);

        // executando a consulta
        $stmt->execute();
        unset($con);
        $row = $stmt->fetchAll();
        if(count($row) == 1){
            return $row[0]['LocUndAuto'];
        }
    }
}