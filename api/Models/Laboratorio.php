<?php
autoload('Bd_Conexao');
class Models_Laboratorio {

    public function retornaAccountKey($id) {

        $con = getInstance();

        $sql = 'SELECT PesPesLabGtwAccountKey FROM pespeslabgtw WHERE PesPesLabAuto = :id';
        $stmt = $con->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $row = $stmt->fetchAll();

        if(count($row) == 1){
            $retorno = array(
                'success' => true,
                'key' => $row[0]['PesPesLabGtwAccountKey']
            );
        }else{
            $retorno = array('success' => false);
        }

        unset($con);
        return $retorno;
    }

    public function salvaAccountKey($respSplit) {
        /*
         * {
    "Success": true,
    "AccountKey": "593dac82-f2dd-44a4-93ad-b3561436815b",
    "LegalName": "Arthur Dent",
    "DbaName": "Arthur",
    "AccountReference": "GenericReference",
    "DocumentNumber": "123456788",
    "DocumentType": 1,
    "Acquirer": {
        "AcquirerAffiliationKey": null,
        "AcquireCode": null,
        "AcquirerName": null
    },
    "RequestKey": "000f41a7-94d6-4521-9077-3b466cf76bb7",
    "Errors": []
}
         */
        $con = getInstance();

        $sql = 'UPDATE pespeslabgtw SET PesPesLabGtwAccountKey = :accountKey, PesPesLabGtwAcquireCode = :acquireCode, PesPesLabGtwAcquirerAffiliationKey = :acquireKey WHERE PesPesLabAuto = :idLab';

        $stmt = $con->prepare($sql);

        $stmt->bindParam(':accountKey', $respSplit->AccountKey);
        $stmt->bindParam(':acquireCode', $respSplit->Acquirer->AcquireCode);
        $stmt->bindParam(':acquireKey', $respSplit->Acquirer->AcquirerAffiliationKey);
        $stmt->bindParam(':idLab', $respSplit->AccountReference);

        $stmt->execute();

        unset($con);

        if($stmt->rowCount() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function listaLaboratorios($id) {
        $con = getInstance();

        /*
         * Busca o laboratorio cujo ID foi passado por parametro
         * Busca a sigla do estado deste laboratorio
         * Busca os ultimos valores, referentes ao exame, valor de cada via e valor do parceiro
         */
        $sql = 'Select p.PesPesLabAuto, p.PesPesLabIndUCT, p.PesPesLabNome, l.LocUndSigla, az.VdaTPrcValExm, az.VdaTPrcValVias, az.VdaTPrcValParc '
                . 'FROM pespeslab as p '
                . 'INNER JOIN locund as l ON p.LocUndAuto = l.LocUndAuto '
                . 'INNER JOIN azvdatprcval as az ON 1 = 1 '
                . 'WHERE PesPesLabAuto = :id '
                . 'ORDER BY PesPesLabNome ASC, VdaTPrcValData DESC Limit 1;';
        $stmt = $con->prepare($sql);

        $stmt->bindParam(':id', $id);

        // executando a consulta
        $stmt->execute();

//falta verificar se a consulta gerou resultado

        $row = $stmt->fetchAll();
        if(count($row) == 1){
            $retorno['status'] = 200;
            $retorno['lab'] = array(
                'id' => $row[0]['PesPesLabAuto'],
                'nome' => $row[0]['PesPesLabNome'],
                'uct' => strtoupper($row[0]['PesPesLabIndUCT']),
                'uf' => $row[0]['LocUndSigla']
            );

            $retorno['val'] = array(
                'exame' => $row[0]['VdaTPrcValExm'],
                'via' => $row[0]['VdaTPrcValVias'],
                'parceiro' => $row[0]['VdaTPrcValParc']
            );
        }else{
            $retorno['status'] = 401;
        }

        unset($con);
        return $retorno;
    }
}
