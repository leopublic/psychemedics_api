<?php
class Controller
{
    public function trataNotificacao($xml) {
        autoload('Models_Declaracao');
        $declaracaoDao = new Models_Declaracao();
        if (!($idLab = $declaracaoDao->buscaTransactionKey($key))) {
            return false;
        }
    }

    public function walletAtualizarConta($body) {
        autoload('Pg_Split');
        $split = new Pg_Split();
        if (!($resp = $split->trataAtualizarDados($body))) {
        }
        if (!$split->atualizarDados($body)) {
        }
    }
    
    public function walletConsultarConta($id) {
        autoload('Models_Laboratorio');
        $lab = new Models_Laboratorio();
        $respLab = $lab->retornaAccountKey($id);
        if (!$respLab['success']) {
            return FALSE;
        }
        autoload('Pg_Split');
        $split = new Pg_Split();
        $retorno = $split->consultaConta($respLab['key']);
        return $retorno;
    }
    
    public function walletAddSplit($response, $lastId, $accountKey, $cobranca) {
        autoload('Pg_Split');
        $split = new Pg_Split();
        $respSplit = $split->realizarSplit($response, $lastId, $accountKey, $cobranca);
        print_r($respSplit);
        exit();
        if (!$split->trataRespostaCriarSplit($respSplit)) {
            return FALSE;
        }
        autoload('Models_Declaracao');
        $declaracao = new Models_Declaracao();
        return $declaracao->salvaChaveSplit($respSplit);
    }
    
    public function walletAddLaboratorio($body) {
        autoload('Pg_Split');
        $split = new Pg_Split();
        if (!$split->verificarCriarConta($body)) {
            return array('status' => 301, 'msgErro' => 'Preencha todos os dados corretamente.');
        }
        $respSplit = $split->criarConta($body);
        if (!($resp = $split->trataRespostaCriarConta($respSplit))) {
            return array('status' => 303, 'msgErro' => 'Ocorreu um erro na requisição. Por favor tente novamente.', 'itemErro' => $respSplit);
        } 
        else if (is_object($resp) || is_array($resp)) {
            return array('status' => 303, 'msgErro' => $resp);
        }
        autoload('Models_Laboratorio');
        $lab = new Models_Laboratorio();
        $resp = $lab->salvaAccountKey($respSplit);
        if ($resp) {
            return array('status' => 201);
        } 
        else {
            return array('status' => 401);
        }
    }
    
    public function salvaDadosFormulario($dados) {
        date_default_timezone_set('America/Sao_Paulo');
        autoload('Pg_Payment');
        autoload('Models_Declaracao');
        $pay = new Pg_Payment();
        $response = $pay->AuthAndCapture($dados);
        if ($response['success']) {
            $declaracao = new Models_Declaracao();
            $lastId = $declaracao->enviaDeclaracao($dados, $response);
            autoload('Models_Laboratorio');
            $lab = new Models_Laboratorio();
            $accountKey = $lab->retornaAccountKey($dados->lab->id);
            $respSplit = $this->walletAddSplit($response, $lastId, $accountKey, $dados->cobranca);
        }
        if ($respSplit) {
            echo 'foi';
        } 
        else {
            echo 'nao foi';
        }
    }
    
    public function buscaCep($cep) {
        autoload('Util_BuscaCep');
        $buscaCep = new Util_BuscaCep();
        return $buscaCep->buscaCep($cep);
    }
    
    public function buscaLaboratorios($id) {
        autoload('Models_Laboratorio');
        $labDao = new Models_Laboratorio();
        return $labDao->listaLaboratorios($id);
    }
    
    public function calculaFrete($cep, $tipoEnvio) {
        autoload('Util_CalculaFrete');
        $calc = new Util_CalculaFrete($cep, $tipoEnvio);
        return $calc->calcular();
    }
}