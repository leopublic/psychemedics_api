<?php
class Pg_Split
{
    private $smartKey = '50BF2616-5FFE-4E35-87A5-625A6BA7069D';
    private $url = 'http://smartwalletstaging.mundipagg.com/';

    public function atualizarDados($body) {
        $url = $this->url . 'Account/' . $body->AccountKey;
        $data = json_encode($body, JSON_UNESCAPED_UNICODE);
        return $this->requisicaoSmartWallet($url, $data, 'POST');
    }

    public function trataAtualizarDados($body) {

    }

    public function realizarSplit($response, $lastId, $accountKey, $cobranca) {
        $url = $this->url . 'Transaction/';
        $valorTotal = floatval(str_replace(',', '.', $cobranca->total)) * 100;
        $parceiro = floatval(str_replace(',', '.', $cobranca->parceiro)) * 100;
        $valorPsy = $valorTotal - $parceiro;
        $requisicao = array('CreditItemCollection' => array(array('OneTransactionKey' => $response['metodo']['transactionKey'], 'AccountKey' => $accountKey, 'Amount' => $valorTotal, 'CurrencyIsoCode' => 'BRL', 'Description' => 'Split referente a declaracao ID #' . $lastId, 'FeeType' => 'Flat', 'FeeValue' => $valorPsy, 'HoldTransaction' => false, 'ItemReference' => $lastId, 'SplitTransaction' => true)));
        $data = json_encode($requisicao, JSON_UNESCAPED_UNICODE);
        return $this->requisicaoSmartWallet($url, $data, 'POST');
    }

    public function trataRespostaCriarSplit($respSplit) {
        if (!is_object($respSplit)) {
            return false;
        }
        if (!isset($respSplit->CreditFinancialMovementCollection)) {
            return false;
        }
        if (!isset($respSplit->CreditFinancialMovementCollection[0]->FinancialMovementKey)) {
            return false;
        }
        if (!isset($respSplit->CreditFinancialMovementCollection[0]->ItemReference)) {
            return false;
        }
        if (isset($respSplit->Errors)) {
            return FALSE;
        }
        return TRUE;
    }

    public function criarConta($body) {
        $url = $this->url . 'Account/';
        $body->SmartWalletKey = $this->smartKey;
        $data = json_encode($body, JSON_UNESCAPED_UNICODE);
        return $this->requisicaoSmartWallet($url, $data, 'POST');
    }

    public function trataRespostaCriarConta($resp) {
        if (!is_object($resp)) {
            return false;
        }
        if (!isset($resp->AccountKey)) {
            return false;
        }
        if (count($resp->Errors) > 0) {
            return $resp->Errors;
        }
        return TRUE;
    }

    public function verificarCriarConta($body) {
        if (!is_object($body)) {
            return false;
        }
        if (!isset($body->AccountReference)) {
            return false;
        }
        if (!isset($body->Addresses)) {
            return false;
        }
        if (!isset($body->DbaName)) {
            return false;
        }
        if (!isset($body->DocumentNumber)) {
            return false;
        }
        if (!isset($body->FinancialDetails)) {
            return false;
        }
        return TRUE;
    }

    public function consultaConta($key) {
        $url = $this->url . 'Account/' . $key;
        return $this->requisicaoSmartWallet($url);
    }

    private function requisicaoSmartWallet($url, $data = false, $method = false) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if ($data) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, Array("Content-Type: application/json;charset=utf-8", "SmartWalletKey:" . $this->smartKey));
        switch ($method) {
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, true);
                break;

            case 'PUT':
                curl_setopt($curl, CURLOPT_PUT, true);
                break;

            case 'DELETE':
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;

            default:
                break;
        }
        $resp = curl_exec($curl);
        curl_close($curl);
        return json_decode($resp);
    }
}
