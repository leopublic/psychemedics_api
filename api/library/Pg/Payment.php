<?php
autoload('Mundipagg_MundiPaggServiceConfiguration');
autoload('Mundipagg_MundiPaggService_ServiceClient_MundiPaggSoapServiceClient');
class Pg_Payment
{
    public function AuthAndCapture($dados) {
        $t = str_replace(',', '.', $dados->cobranca->total);
        $valorTotal = floatval($t) * 100;
        $orderRequest = new CreateOrderRequest();
        $orderRequest->CurrencyIsoEnum = "BRL";
        $orderRequest->AmountInCents = $valorTotal;
        $orderRequest->Retries = 2;
        $orderRequest->OrderReference = "";
        if ($dados->pgMetodo == 'Boleto') {
            $orderRequest->BoletoTransactionCollection = array($this->montaBoleto($dados, $valorTotal));
        } 
        else {
            $orderRequest->CreditCardTransactionCollection = array($this->montaCreditCard($dados, $valorTotal));
        }
        $orderRequest->Buyer = $this->montaBuyer($dados);
        $orderRequest->ShoppingCartCollection = array($this->montaShopCart($valorTotal));
        $client = new MundiPaggSoapServiceClient('LOCAL', null, true);
        $orderResponse = $client->CreateOrder($orderRequest);
        $retorno = $this->trataRespostaGateway($orderResponse, $dados->pgMetodo);
        return $retorno;
    }
    private function trataRespostaGateway($orderResponse, $metodo) {
        if (!$orderResponse->Success) {
            return $this->trataErro($orderResponse);
        }
        if ($metodo == 'Boleto') {
            $boleto = $orderResponse->BoletoTransactionResultCollection[0];
            $metodoResponse = array('tipo' => 'Boleto', 'codigoBarra' => $boleto->Barcode, 'url' => $boleto->BoletoUrl, 'transactionKey' => $boleto->TransactionKey, 'status' => $boleto->BoletoTransactionStatusEnum);
        } 
        else {
            $cartao = $orderResponse->CreditCardTransactionResultCollection[0];
            $metodoResponse = array('tipo' => 'Cartao', 'transactionKey' => $cartao->TransactionKey, 'status' => $cartao->CreditCardTransactionStatusEnum);
        }
        if (!($indPedido = $this->trataStatusTransacao($metodoResponse['status']))) {
            $success = false;
            $status = 400;
        } 
        else {
            $success = $orderResponse->Success;
            $status = 200;
        }
        return array('status' => $status, 'success' => $success, 'statusOrder' => $orderResponse->OrderStatusEnum, 'orderKey' => $orderResponse->OrderKey, 'metodo' => $metodoResponse, 'indPedido' => $indPedido);
    }
    private function trataErro($orderResponse) {
        $err = $orderResponse->ErrorReport;
        for ($i = 0; $i < count($err->ErrorItemCollection); $i++) {
            $errColection[$i] = array('code' => $err->ErrorItemCollection[$i]->ErrorCode, 'field' => $err->ErrorItemCollection[$i]->ErrorField, 'Description' => $err->ErrorItemCollection[$i]->Description, 'status' => $err->ErrorItemCollection[$i]->SeverityCodeEnum);
        }
        return array('status' => 400, 'success' => false, 'category' => $err->category, 'errColection' => $errColection);
    }
    private function trataStatusTransacao($status) {
        switch ($status) {
            case 'Generated':
                return 'A';
            case 'Viewed':
                return 'A';
            case 'UnderPaid':
                return false;
            case 'OverPaid':
                return 'P';
            case 'Paid':
                return 'P';
            case 'Voided':
                return false;
            case 'WithError':
                return false;
            case 'AuthorizedPendingCapture':
                return 'A';
            case 'NotAuthorized':
                return false;
            case 'ChargebackPreview':
                return 'A';
            case 'RefundPreview':
                return false;
            case 'DepositPreview':
                return 'A';
            case 'Captured':
                return 'P';
            case 'PartialCapture':
                return 'A';
            case 'Refunded':
                return false;
            case 'Deposited':
                return 'P';
            case 'OpenedPendindAuth':
                return 'A';
            case 'Chargedback':
                return false;
            default:
                return false;
        }
    }
    private function montaBoleto($dados, $valorTotal) {
        $boletoTransaction1 = new BoletoTransaction();
        $boletoTransaction1->AmountInCents = $valorTotal;
        $boletoTransaction1->NossoNumero = date('YmdHis') . $dados->lab->id;
        $boletoTransaction1->DaysToAddInBoletoExpirationDate = 5;
        return $boletoTransaction1;
    }
    private function montaCreditCard($dados, $valorTotal) {
        $numCartao = str_replace('.', '', $dados->pgCartao->numero);
        $arr = explode('/', $dados->pgCartao->vencimento);
        $mes = $arr[0];
        $ano = $arr[1];
        $ccTransaction1 = new CreditCardTransaction();
        $ccTransaction1->AmountInCents = $valorTotal;
        $ccTransaction1->CreditCardNumber = $numCartao;
        $ccTransaction1->HolderName = $dados->pgCartao->nome;
        $ccTransaction1->SecurityCode = $dados->pgCartao->codigo;
        $ccTransaction1->ExpMonth = $mes;
        $ccTransaction1->ExpYear = $ano;
        $ccTransaction1->CreditCardBrandEnum = $dados->pgCartao->bandeira;
        $ccTransaction1->PaymentMethodCode = 1;
        $ccTransaction1->CreditCardOperationEnum = "AuthAndCapture";
        return $ccTransaction1;
    }
    private function montaShopCart($valorTotal) {
        $shopCart = new ShoppingCart();
        $shopCartItem = new ShoppingCartItem();
        $shopCartItem->Description = "Exame Toxicológico CNH - " . date('Y');
        $shopCartItem->Name = "Exame Toxicológico";
        $shopCartItem->Quantity = 1;
        $shopCartItem->TotalCostInCents = $valorTotal;
        $shopCartItem->UnitCostInCents = $valorTotal;
        $shopCartItem->ItemReference = "REFExame";
        $shopCart->ShoppingCartItemCollection = array($shopCartItem);
        return $shopCart;
    }
    private function montaBuyer($dados) {
        $buyer = new Buyer();
        $buyer->Email = $dados->notaFiscal->email;
        $buyer->MobilePhone = $dados->notaFiscal->fone;
        $buyer->Name = $dados->notaFiscal->nome;
        $buyer->PersonTypeEnum = 'Person';
        $buyer->TaxDocumentNumber = $dados->notaFiscal->doc;
        $buyer->TaxDocumentTypeEnum = $dados->notaFiscal->tipoDoc;
        $addr1 = new BuyerAddress();
        $addr1->AddressTypeEnum = AddressTypeEnum::Residential;
        $addr1->City = $dados->endCobranca->cidade;
        $addr1->Complement = $dados->endCobranca->complemento;
        $addr1->CountryEnum = CountryEnum::Brazil;
        $addr1->Number = $dados->endCobranca->numero;
        $addr1->State = $dados->endCobranca->uf;
        $addr1->Street = $dados->endCobranca->logradouro;
        $addr1->ZipCode = $dados->endCobranca->cep;
        $buyer->BuyerAddressCollection = array($addr1);
        return $buyer;
    }
    public function consultaTransacao($orderKey) {
        $query = new QueryOrderRequest();
        $query->OrderKey = $orderKey;
        $query->MerchantKey = '092cfb63-9574-4e9a-817f-835ae8d9eeaa';
    }
}
