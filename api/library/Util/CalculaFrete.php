<?php
class Util_CalculaFrete
{
    private $url;

    public function __construct($cep, $tipoEnvio) {
        switch ($tipoEnvio) {
            case 'PAC':
                $tipoEnvio = '41106';
                break;

            case 'SEDEX':
                $tipoEnvio = '40010';
                break;
        }
        $cep = str_replace('-', '', $cep);
        $this->url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?';
        $arr = array('nCdEmpresa=', 'sDsSenha=', 'sCepOrigem=01047912', 'sCepDestino=' . $cep, 'nVlPeso=0.3', 'nCdFormato=3', 'nVlComprimento=26', 'nVlAltura=0', 'nVlLargura=36', 'sCdMaoPropria=n', 'nVlValorDeclarado=0', 'sCdAvisoRecebimento=n', 'nCdServico=' . $tipoEnvio, 'nVlDiametro=0', 'StrRetorno=xml', 'nIndicaCalculo=3');
        $this->url.= implode('&', $arr);
    }

    public function calcular() {
        $xml = simplexml_load_file($this->url);
        if ($xml->cServico->Erro == 0) {
            $retorno['status'] = 200;
            $retorno['valor'] = $xml->cServico->Valor;
            $retorno['prazo'] = $xml->cServico->PrazoEntrega;
        }
        else {
            $retorno['status'] = 300;
        }
        return $retorno;
    }
}
