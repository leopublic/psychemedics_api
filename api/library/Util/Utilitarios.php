<?php
class Util_Utilitarios
{
    public function validaEmail($email) {
        $conta = "^[a-zA-Z0-9\._-]+@";
        $domino = "[a-zA-Z0-9\._-]+.";
        $extensao = "([a-zA-Z]{2,4})$";
        $pattern = $conta . $domino . $extensao;
        if (ereg($pattern, $email)) return true;
        else return false;
    }

    public function limpaString($mensagem) {
        $nopermitidos = array('\\');
        $mensagem = str_replace($nopermitidos, "", $mensagem);
        return $mensagem;
    }

    public function converterData($data, $in = true) {
        if ($in == true) {
            $newData = explode("/", $data);
            $newData = array_reverse($newData);
            return implode("-", $newData);
        }
        else {
            $newData = explode("-", $data);
            $newData = array_reverse($newData);
            return implode("/", $newData);
        }
    }

    public function retira_acentos($texto) {
        $array1 = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç", "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç", "!", "?", "=", "+", "(", ")", "\"", "'", "/");
        $array2 = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c", "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", "", "", "", "", "", "", "", "", "");
        return str_replace($array1, $array2, $texto);
    }

    public function listaEstados($selected = "") {
        $arrEstados = array('AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MS', 'MT', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO');
        $retorno = '<select name="uf" style="width: 155px; margin-top: 5px;">';
        for ($i = 0; $i < count($arrEstados); $i++) {
            ($selected == $arrEstados[$i]) ? $c = ' selected ' : $c = '';
            $retorno.= '<option value="' . $arrEstados[$i] . '" ' . $c . '>' . $arrEstados[$i] . '</option>';
        }
        $retorno.= '</select>';
        return $retorno;
    }

    public function validarCPF($cpf) {
        $cpf = str_pad(preg_match("/^[^0-9]/", "", $cpf), 11, "0", STR_PAD_LEFT);
        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
            return false;
        }
        else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d+= $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }

    public function validarCNPJ($cnpj) {
        $j = 0;
        for ($i = 0; $i < (strlen($cnpj)); $i++) {
            if (is_numeric($cnpj[$i])) {
                $num[$j] = $cnpj[$i];
                $j++;
            }
        }
        if (count($num) != 14) {
            $isCnpjValid = false;
        }
        if ($num[0] == 0 && $num[1] == 0 && $num[2] == 0 && $num[3] == 0 && $num[4] == 0 && $num[5] == 0 && $num[6] == 0 && $num[7] == 0 && $num[8] == 0 && $num[9] == 0 && $num[10] == 0 && $num[11] == 0) {
            $isCnpjValid = false;
        }
        else {
            $j = 5;
            for ($i = 0; $i < 4; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 4; $i < 12; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            }
            else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[12]) {
                $isCnpjValid = false;
            }
        }
        if (!isset($isCnpjValid)) {
            $j = 6;
            for ($i = 0; $i < 5; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $j = 9;
            for ($i = 5; $i < 13; $i++) {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2) {
                $dg = 0;
            }
            else {
                $dg = 11 - $resto;
            }
            if ($dg != $num[13]) {
                $isCnpjValid = false;
            }
            else {
                $isCnpjValid = true;
            }
        }
        return $isCnpjValid;
    }

    public function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        }
        else if (is_array($input)) {
            foreach ($input as & $value) {
                $this->utf8_encode_deep($value);
            }
            unset($value);
        }
        else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            foreach ($vars as $var) {
                $this->utf8_encode_deep($input->$var);
            }
        }
    }

    public function utf8_decode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_decode($input);
        }
        else if (is_array($input)) {
            foreach ($input as & $value) {
                $this->utf8_decode_deep($value);
            }
            unset($value);
        }
        else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            foreach ($vars as $var) {
                $this->utf8_decode_deep($input->$var);
            }
        }
    }
}