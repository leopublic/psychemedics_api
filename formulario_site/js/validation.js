function validaForm() {
    var resp = '';
    $('label').children().removeClass('border-error');
    if ($('.formLaboratorio').val() == '') {
        resp += 'Escolha o laboratório em que será feito a coleta.<br/>';
    }
    if ($('.formDoadorNome').val() == '') {
        $('.formDoadorNome').addClass('border-error');
        resp += 'Preencha o nome corretamente.<br/>';
    }
    if ($('.formDoadorCpf').val() == '' || !TestaCPF($('.formDoadorCpf').val())) {
        $('.formDoadorCpf').addClass('border-error');
        resp += 'Preencha o CPF do doador corretamente.<br/>';
    }
    if ($('.formNotaCpf').val() == '' && $('.formNotaCnpj').val() == '') {
        $('.formNotaCpf').addClass('border-error');
        $('.formNotaCnpj').addClass('border-error');
        resp += 'Preencha o CPF ou CNPJ da nota fiscal<br/>';
    } else {
        if ($('.formNotaCpf').val() != '' && !TestaCPF($('.formNotaCpf').val())) {
            $('.formNotaCpf').addClass('border-error');
            resp += 'Digite um CPF válido<br/>';
        }
        if ($('.formNotaCnpj').val() != '' && !validarCNPJ($('.formNotaCnpj').val())) {
            $('.formNotaCnpj').addClass('border-error');
            resp += 'Digite um CNPJ válido<br/>';
        }
    }
    if ($('.formNotaNome').val() == '') {
        $('.formNotaNome').addClass('border-error');
        resp += 'Preencha o Nome corretamente.<br/>';
    }
    if ($('.formNotaEmail').val() == '' || !ValidaEmail($('.formNotaEmail').val())) {
        $('.formNotaEmail').addClass('border-error');
        resp += 'Preencha o Email corretamente.<br/>';
    }
    if ($('.formCobrancaCep').val() == '' || !ValidaCep($('.formCobrancaCep').val())) {
        $('.formCobrancaCep').addClass('border-error');
        resp += 'Preencha um CEP válido.<br/>';
    } else {
        if ($('.formCobrancaCidade').val() == '') {
            $('.formCobrancaCidade').addClass('border-error');
            resp += 'Preencha a cidade corretamente.<br/>';
        }
        if ($('.formCobrancaBairro').val() == '') {
            $('.formCobrancaBairro').addClass('border-error');
            resp += 'Preencha o bairro corretamente.<br/>';
        }
        if ($('.formCobrancaLogradouro').val() == '') {
            $('.formCobrancaLogradouro').addClass('border-error');
            resp += 'Preencha o logradouro corretamente.<br/>';
        }
        if ($('.formCobrancaNumero').val() == '') {
            $('.formCobrancaNumero').addClass('border-error');
            resp += 'Preencha o número corretamente.<br/>';
        }
    }
    if ($('.formLaudo').val() == '2') {
        if ($('.formLaudoCep').val() == '' || !ValidaCep($('.formLaudoCep').val())) {
            $('.formLaudoCep').addClass('border-error');
            resp += 'Preencha um CEP válido<br/>';
        } else {
            if ($('.formLaudoCidade').val() == '') {
                $('.formLaudoCidade').addClass('border-error');
                resp += 'Digite o nome da cidade corretamente<br/>';
            }
            if ($('.formLaudoBairro').val() == '') {
                $('.formLaudoBairro').addClass('border-error');
                resp += 'Digite o nome do bairro corretamente<br/>';
            }
            if ($('.formLaudoLogradouro').val() == '') {
                $('.formLaudoLogradouro').addClass('border-error');
                resp += 'Digite o logradouro corretamente<br/>';
            }
            if ($('.formLaudoNumero').val() == '') {
                $('.formLaudoNumero').addClass('border-error');
                resp += 'Digite o número corretamente<br/>';
            }
        }
    }
    $(".respForm").html(resp);
    if (resp != '') {
        return false;
    }
    if (Valor.frete == undefined) {
        return reqCalcFrete();
    }
    return true;
}

function montaDadosForm() {
    if ($('.formPgMetodoCartao').is(":checked")) {
        var metodo = 'Cartao';
    } else {
        var metodo = 'Boleto';
    }
    if ($('.formFreteTipoPac').is(':checked')) {
        var tipoFrete = 'PAC';
    } else {
        var tipoFrete = 'SEDEX';
    }
    if ($('.formNotaCpf').val() != '') {
        var tipoDoc = 'CPF';
        var doc = $('.formNotaCpf').val();
    } else {
        var tipoDoc = 'CNPJ';
        var doc = $('.formNotaCnpj').val();
    }
    return {
        pgMetodo: metodo,
        lab: {
            id: $('.formLaboratorioId').val(),
            nome: $('.formLaboratorio').val(),
            uf: $('.formLaboratorioUf').val()
        },
        doador: {
            nome: $('.formDoadorNome').val(),
            cpf: $('.formDoadorCpf').val(),
            celular: $('.formDoadorCelular').val()
        },
        notaFiscal: {
            tipoDoc: tipoDoc,
            doc: doc,
            nome: $('.formNotaNome').val(),
            email: $('.formNotaEmail').val(),
            fone: $('.formNotaFone').val(),
            envioFluxo: $('.formNotaEnvioFluxo').val()
        },
        laudo: {
            envioLaudo: $('.formLaudo').val(),
            medicoVisualiza: $('.formLaudoMedicoRecebe').val(),
            quantidadeVias: $('.formLaudoVias').val()
        },
        endCobranca: {
            cep: $('.formCobrancaCep').val(),
            uf: $('.formCobrancaUf').val(),
            cidade: $('.formCobrancaCidade').val(),
            bairro: $('.formCobrancaBairro').val(),
            logradouro: $('.formCobrancaLogradouro').val(),
            numero: $('.formCobrancaNumero').val(),
            complemento: $('.formCobrancaComplemento').val()
        },
        endInformado: {
            cep: $('.formLaudoCep').val(),
            uf: $('.formLaudoUf').val(),
            cidade: $('.formLaudoCidade').val(),
            bairro: $('.formLaudoBairro').val(),
            logradouro: $('.formLaudoLogradouro').val(),
            numero: $('.formLaudoNumero').val(),
            complemento: $('.formLaudoComplemento').val()
        },
        cobranca: {
            exame: $('.formPgExame').html(),
            totalVias: $('.formPgVias').html(),
            via: Valor.via,
            frete: $('.formPgFrete').html(),
            tipoFrete: tipoFrete,
            parceiro: $('.formPgParceiro').html(),
            total: $('.formPgTotal').html()
        },
        pgCartao: {
            nome: $('.formPgCartaoNome').val(),
            numero: $('.formPgCartaoNumero').val(),
            codigo: $('.formPgCartaoCodigo').val(),
            vencimento: $('.formPgCartaoVencimento').val(),
            bandeira: $('.formPgCartaoBandeira').val()
        }
    };
}

function calcFrete() {
    $('.formFreteBtn').click(function() {
        $('.formFreteTipo').html('');
        $('.formFreteValor').html('');
        $('.formFretePrazo').html('');
        reqCalcFrete();
    });
    $('.formFreteTipoPac').click(function() {
        if ($('.formFreteTipo').html() == '' || $('.formFreteTipo').html() != 'PAC') {
            $('.formFreteTipo').html('');
            $('.formFreteValor').html('');
            $('.formFretePrazo').html('');
            reqCalcFrete();
        }
    });
    $('.formFreteTipoSedex').click(function() {
        if ($('.formFreteTipo').html() == '' || $('.formFreteTipo').html() != 'SEDEX') {
            $('.formFreteTipo').html('');
            $('.formFreteValor').html('');
            $('.formFretePrazo').html('');
            reqCalcFrete();
        }
    });
}

function validaFormPg() {
    if ($('.formPgMetodoCartao').is(":checked")) {
        var resp = '';
        $('label').children().removeClass('border-error');
        if ($('.formPgCartaoNome').val() == '') {
            $('.formPgCartaoNome').addClass('border-error');
            resp += 'Preencha o nome corretamente.</br>';
        }
        if ($('.formPgCartaoNumero').val() == '') {
            $('.formPgCartaoNumero').addClass('border-error');
            resp += 'Preencha o número do cartão.</br>';
        }
        if ($('.formPgCartaoBandeira').val() == '') {
            $('.formPgCartaoBandeira').addClass('border-error');
            resp += 'Escolha a bandeira do cartão.</br>';
        }
        if ($('.formPgCartaoCodigo').val() == '') {
            $('.formPgCartaoCodigo').addClass('border-error');
            resp += 'Preencha o código do cartão.</br>';
        }
        if ($('.formPgCartaoVencimento').val() == '' || cartaoInvalido($('.formPgCartaoVencimento').val())) {
            $('.formPgCartaoVencimento').addClass('border-error');
            resp += 'Preencha o vencimento.</br>';
        }
        $('.respFormPg').html(resp);
        if (resp != '') {
            return false;
        }
    }
    return true;
}

function validaFormCartao() {
    $(".formPgCartaoNome").keyup(function() {
        var valor = $(this).val().replace(/[^a-zA-Z\s]+/g, '');
        $(this).val(valor);
    });
    $(".formPgCartaoCodigo").keyup(function() {
        var valor = $(this).val().replace(/[^0-9]+/g, '');
        $(this).val(valor);
    });
}

function cartaoInvalido(validade) {
    var data = new Date();
    var ano = data.getFullYear().toString();
    ano = ano.substr(2, 3);
    var arr = validade.split('/');
    if (arr[0] < 1 || arr[0] > 12) {
        return true;
    }
    if (arr[1] < ano) {
        return true;
    }
    return false;
}

function calcularPg() {
    if (Valor == null) {
        resp = 'Faça o calculo do frete corretamente.<br/>';
        $('.respForm').html(resp);
        return false;
    }
    if ($('.formLaudo').val() == null) {
        resp = 'Faça o calculo do frete corretamente..<br/>';
        $('.respForm').html(resp);
        return false;
    } else if ($('.formLaudo').val() != 0 && $('.formFreteValor').html() == '') {
        resp = 'Faça o calculo do frete corretamente...<br/>';
        $('.respForm').html(resp);
        return false;
    }
    var exame = parseFloat(Valor.exame).toFixed(2);
    var parceiro = parseFloat(Valor.parceiro).toFixed(2);
    if ($('.formLaudo').val() == 0) {
        var qtdVias = '0';
        var via = '0,0';
        var frete = '0,0';
    } else {
        var qtdVias = $('.formLaudoVias').val();
        var via = (parseFloat(qtdVias) * parseFloat(Valor.via)).toFixed(2);
        var frete = parseFloat(Valor.frete).toFixed(2);
    }
    var total = (parseFloat(exame) + parseFloat(via) + parseFloat(frete) + parseFloat(parceiro)).toFixed(2);
    $('.formPgExame').html((exame).replace(".", ","));
    $('.formPgVias').html((via).replace(".", ","));
    $('.formPgFrete').html((frete).replace(".", ","));
    $('.formPgParceiro').html((parceiro).replace(".", ","));
    $('.formPgTotal').html((total).replace(".", ","));
    return true;
}

function exibirPg() {
    if (calcularPg()) {
        $('.form1').slideUp(300);
        $('.form2').slideDown(300);
        validaFormCartao();
    }
}

function btnVoltar() {
    $('.form2').slideUp(300);
    $('.form1').slideDown(300);
}

function changeOpcaoEnvioLaudo() {
    $('.formLaudoMedicoRecebe').change(function() {
        if ($('.formLaudoMedicoRecebe').val() == "N") {
            $(".formLaudoNaoRecebe").attr('disabled', 'disabled');
            if ($('.formLaudo').val() == null) {
                $('.formLaudo').val('1');
                if ($('.formLaudoVias').parent('label').hasClass('hidden')) {
                    $('.formLaudoVias').val('1').parent('label').removeClass('hidden');
                }
                $('.formFieldFrete').slideDown(300);
            }
        } else {
            $(".formLaudoNaoRecebe").removeAttr('disabled');
        }
    });
}

function envioLaudoEndereco() {
    $('.formLaudoCep').parents('div').hide();
    $(".formLaudo").change(function() {
        if ($(this).val() == 2) {
            $('.formLaudoCep').parents('div').slideDown(300);
            $('.formLaudoCep').removeAttr('disabled');
        } else {
            $('.formLaudoCep').parents('div').slideUp(300);
            $('.formLaudoCep').attr('disabled', 'disabled').val('');
            $('.formLaudoEndereco').slideUp(300);
        }
        if ($(this).val() == 0) {
            $('.formFieldFrete').slideUp(300);
            $('.formFreteValorPrazo').addClass('hidden');
            $('.formLaudoVias').val('0').parent('label').addClass('hidden');
        } else {
            if ($('.formLaudoVias').parent('label').hasClass('hidden')) {
                $('.formLaudoVias').val('1').parent('label').removeClass('hidden');
            }
            $('.formFieldFrete').slideDown(300);
        }
    });
}

function toggleIdentidade() {
    $('.formRadioIdentidade').click(function() {
        var value = $(this).val();
        if (value == 'cpf') {
            $('.formNotaCpf').show();
            $('.formNotaCnpj').hide().val('');
        } else {
            $('.formNotaCnpj').show();
            $('.formNotaCpf').hide().val('');
        }
    });
    $('.formPgMetodoCartao').click(function() {
        $('.formPgCartao').slideDown(300);
    });
    $('.formPgMetodoBoleto').click(function() {
        $('.formPgCartao').slideUp(300);
    });
}

function labInvalido() {
    alert('Nenhum laboratório encontrada. Escolha um laboratório válido.');
    $(location).attr('href', 'http://www.exametoxicologicocnh.com.br/');
}

function ValidaEmail(pEmail) {
    var posicaoArroba, tamanhoEmail;
    tamanhoEmail = pEmail.length;
    if ((posicaoArroba = pEmail.indexOf("@")) < 1) {
        return false;
    }
    if (pEmail.indexOf("@", posicaoArroba + 1) >= 0) {
        return false;
    }
    if (posicaoArroba == (tamanhoEmail - 1)) {
        return false;
    }
    if (pEmail.indexOf(".") == 0) {
        return false;
    }
    if (pEmail.lastIndexOf(".") == (tamanhoEmail - 1)) {
        return false;
    }
    if (pEmail.indexOf("..") >= 0) {
        return false;
    }
    if (pEmail.indexOf("@.") >= 0) {
        return false;
    }
    if (pEmail.indexOf(".", posicaoArroba + 1) < 0) {
        return false;
    }
    if (pEmail.indexOf("$") >= 0) {
        return false;
    }
    if (pEmail.indexOf("#") >= 0) {
        return false;
    }
    return true;
}

function ValidaCep(cep) {
    exp = /\d{2}\d{3}\-\d{3}/;
    if (!exp.test(cep)) {
        return false;
    } else {
        return true;
    }
}

function validarCNPJ(cnpj) {
    cnpj = cnpj.replace(/[^\d]+/g, '');
    if (cnpj == '') return false;
    if (cnpj.length != 14) return false;
    if (cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222" || cnpj == "33333333333333" || cnpj == "44444444444444" || cnpj == "55555555555555" || cnpj == "66666666666666" || cnpj == "77777777777777" || cnpj == "88888888888888" || cnpj == "99999999999999") return false;
    tamanho = cnpj.length - 2
    numeros = cnpj.substring(0, tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0)) return false;
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2) pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1)) return false;
    return true;
}

function TestaCPF(cpf) {
    var strCPF = cpf.replace('.', '').replace('.', '').replace('-', '');
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000") return false;
    for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10))) return false;
    Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;
    if ((Resto == 10) || (Resto == 11)) Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11))) return false;
    return true;
}
jQuery(function($) {
    $.mask.definitions['~'] = '[+-]';
    $('input[type=dat]').mask("99/99").ready(function(event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 7) {
            element.mask("99/99");
        } else {
            element.mask("99/99");
        }
    });
    $('input[type=numcart]').mask("9999.9999.9999.9999").ready(function(event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 7) {
            element.mask("9999.9999.9999.9999");
        } else {
            element.mask("9999.9999.9999.9999");
        }
    });
    $('input[type=cep]').mask("99999-999").ready(function(event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        if (target == undefined) {
            return;
        }
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 10) {
            element.mask("99999-999");
        } else {
            element.mask("99999-999");
        }
    });
    $('input[type=cpf]').mask("999.999.999-99").ready(function(event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 11) {
            element.mask("999.999.999-99");
        } else {
            element.mask("999.999.999-99");
        }
    });
    $('input[type=cnpj]').mask("99.999.999/9999-99").ready(function(event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 14) {
            element.mask("99.999.999/9999-99");
        } else {
            element.mask("99.999.999/9999-99");
        }
    });
    $('input[type=tel]').mask("(99) 9999-9999?9").ready(function(event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        if (target == undefined) {
            return;
        }
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
            $("#formManual_telefone").css('border', 'solid thin #000');
        } else {
            element.mask("(99) 9999-9999?9");
            $("#formManual_telefone").css('border', 'solid thin #000');
        }
    });
});