var SERV = 'http://177.10.154.130:8888/psy_cnh_v05/_api/api/';

function reqEnviarForm() {
    var dados = montaDadosForm();
    var url = SERV + 'enviaFormPrincipal';
    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(dados),
        success: function(data) {
            console.log(data);
            if (data.status == '200') {
                alert('Obrigado!\nVocê receberá um e-mail confirmando seu pedido.');
                if (data.metodo.tipo == "Boleto") {
                    window.open(data.metodo.url);
                }
                $(location).attr('href', 'http://www.exametoxicologicocnh.com.br/');
            } else {
                alert('Ocorreu um erro na transação, por favor, escolha outro cartão de cédito, ou tente novamente mais tarde.');
                $('.formSubmit').val('Efetuar pagamento');
            }
        }
    });
}

function reqCalcFrete() {
    var resp = '';
    if ($('.formLaudo').val() == '1' && ValidaCep($('.formCobrancaCep').val())) {
        var cep = $('.formCobrancaCep').val();
    } else if ($('.formLaudo').val() == '2' && ValidaCep($('.formLaudoCep').val())) {
        var cep = $('.formLaudoCep').val();
    } else {
        return true;
    }
    $('.formFreteBtn').val('Calculando...');
    if ($('.formFreteTipoPac').is(":checked")) {
        var tipoEnvio = $('.formFreteTipoPac').val();
    } else if ($('.formFreteTipoSedex').is(":checked")) {
        var tipoEnvio = $('.formFreteTipoSedex').val();
    }
    var url = SERV + 'calcFrete/' + cep + '/' + tipoEnvio;
    $.ajax({
        type: 'GET',
        url: url,
        success: function(data) {
            $('.formFreteBtn').val('Calcular Frete');
            if (data.status == '200') {
                $('.formFreteTipo').html(tipoEnvio);
                $('.formFreteValor').html(data.valor[0]);
                $('.formFretePrazo').html(data.prazo[0]);
                $('.formFreteValorPrazo').removeClass('hidden');
                if (Valor != null) {
                    Valor.frete = (data.valor[0]).replace(',', '.');
                }
                return true;
            } else {
                return false;
            }
        }
    });
}

function reqBuscaLaboratorios(id) {
    var url = SERV + 'buscaLaboratorios/' + id;
    $.ajax({
        type: 'GET',
        url: url,
        success: function(data) {
            if (data.status == "200") {
                Valor = data.val;
                $('.formLaboratorio').val(data.lab.nome);
                $('.formLaboratorioId').val(data.lab.id);
                $('.formLaboratorioUf').val(data.lab.uf);
                if (data.lab.uct == 'S') {
                    $('.formLaudoUct').removeClass('hidden');
                    if ($('.formLaudoMedicoRecebe').val() == "N") {
                        $(".formLaudoNaoRecebe").attr('disabled', 'disabled');
                    }
                    changeOpcaoEnvioLaudo();
                }
            } else {
                labInvalido();
            }
        }
    });
}

function reqBuscaCep(cep, field) {
    var url = SERV + 'buscaCep/' + cep;
    $.ajax({
        type: 'GET',
        url: url,
        success: function(data) {
            if (data.status == '200') {
                if (field == 'field1') {
                    $(".formCobrancaLogradouro").val(data.result.logradouro);
                    $(".formCobrancaBairro").val(data.result.bairro);
                    $(".formCobrancaUf").val(data.result.uf);
                    $(".formCobrancaCidade").val(data.result.cidade);
                    $(".formEndereco").slideDown(300);
                } else {
                    $(".formLaudoLogradouro").val(data.result.logradouro);
                    $(".formLaudoBairro").val(data.result.bairro);
                    $(".formLaudoUf").val(data.result.uf);
                    $(".formLaudoCidade").val(data.result.cidade);
                    $(".formLaudoEndereco").slideDown(300);
                }
            } else {
                if (field == 'field1') {
                    $(".formEndereco input").val('');
                    $(".formEndereco").slideDown(300);
                } else {
                    $(".formLaudoEndereco input").val('');
                    $(".formLaudoEndereco").slideDown(300);
                }
                alert(data.result.msgErro);
            }
            $(".buscaCep").html('Buscar');
            $(".buscaLaudoCep").html('Buscar');
        }
    });
}
