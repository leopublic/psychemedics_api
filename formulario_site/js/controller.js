var Valor = null;
$(document).ready(function() {
    listarLaboratorios();
    buscaCep();
    toggleIdentidade();
    envioLaudoEndereco();
    calcFrete();
    formBtn();
});

function formBtn() {
    $(".formConcluir").click(function() {
        concluirForm1();
    });
    $('.formVoltar').click(function() {
        btnVoltar();
    });
    $('.formSubmit').click(function() {
        enviarForm();
    });
}

function listarLaboratorios() {
    var url = window.location.search.replace("?", "");
    var items = url.split("&");
    var array = {
        'id': items[0]
    };
    var id = array.id;
    if (id == '' || id == undefined || isNaN(id)) {
        labInvalido();
    } else {
        reqBuscaLaboratorios(id);
    }
}

function enviarForm() {
    if (validaFormPg()) {
        $('.formSubmit').val('Enviando solicitação...');
        reqEnviarForm();
    }
}

function concluirForm1() {
    if (validaForm()) {
        exibirPg();
    }
}

function buscaCep() {
    $(".buscaCep").click(function() {
        var cep = $(".formCobrancaCep").val();
        if (cep != '') {
            $(this).html('Buscando...');
            reqBuscaCep(cep, 'field1');
        }
    });
    $(".buscaLaudoCep").click(function() {
        var cep = $('.formLaudoCep').val();
        if (cep != '') {
            $(this).html('Buscando...');
            reqBuscaCep(cep, 'field2');
        }
    });
}